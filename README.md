# Một số lưu ý về Terria Map
* Các dữ liệu server-side được lưu dưới dạng file JSON, gọi là các file init.
* Những file init đó lưu thông tin về các data GIS được publish. Có thể là lấy từ file có sẵn trên server hoặc từ một backend cụ thể, ví dụ GeoServer WMS.
* Tên lớp dữ liệu và mô tả (Description) trong Terria Map tương ứng với các mục Title và Abstract trong GeoServer.
* File init có cấu trúc điển hình như sau:
~~~~
{
    "catalog": [
        {
            "type": "group",
            "name": "ten_group",
            "items": [
            	{
					"type": "wms",
					"name": "geoserver wms",
					"layers": "layer X",
					"url": ""
				}
            ]
       },
		{
			"type": "wms-getCapabilities",
			"url": "",
			"name": "wms backend",
			"description": ""
		},
        ...
    ],
    "homeCamera": {
       "north": -8,
        "east": 158,
        "south": -45,
        "west": 109
    }
}
~~~~
* Các thông số B-Đ-N-T trong homeCamera là tọa độ 4 đỉnh của bounding box.
* Các loại catalog group (nhóm các item) và catalog item được miêu tả chi tiết trong TerriaJS Docs.
## Cách tạo catalog group mới cho dự án mới
Cách setup thông thường:
~~~~
Dữ liệu GIS trên GeoServer trong một workspace ---> GeoServer Service (vd: WMS)
---> Catalog Group trên Terria Map (Tự động truy xuất các dữ liệu có trong workspace)
~~~~
Các bước:

* Tạo một workspace cho dự án trên GeoServer.
* Tiến hành sửa đổi file _gfd.json_ trong thư mục /root/TerriaMap/wwwroot/init như sau:
    * Tạo thêm một element mới trong array _catalog_.
    * Khóa _name_ của element đó sẽ là tên của dự án.
    * Khóa _type_ sẽ có giá trị _wms-getCapabilities_ (tùy vào loại service, đối chiếu với docs của TerriaJS).
    * Có thể thêm khóa _description_ để mô tả về dự án.
    * Khóa _url_ trỏ tới URL của service trên GeoServer. Ví dụ, đối với WMS thì URL sẽ có dạng: ```/geoserver/tên_workspace/wms```
* Ví dụ:
~~~~
"catalog": [
        {
            "name": "Quang Ninh FRCR project",
            "type": "wms-getCapabilities",
            "url": "/geoserver/QN/wms",
        },
        {
            "name": "du an moi",
            "type": "wms-getCapabilities",
            "description": "abc, def",
            "url": "/geoserver/duanmoi/wms"
        }
]
~~~~